far_to_kel <- function(temp) {
  
  # Separate function checks whether temp 
  # is numeric. If not, function stops and
  # prints the error message.
  
  if (!is.numeric(temp)) {
    stop("Temperature must be a numeric value")
  }
  
  # Alternative function - stopifnot(is.numeric(temp))
  # Prints out an error message
  
  kelvin <- ((temp - 32)*(5/9)) + 273.15
  return(kelvin) 
  # Return statement is optional in R, 
  # should automatically return last object.
  # But best practice is to use it.
}

far_to_kel(0)

# Kelvin to celsius

kel_to_cel <- function(temp) {
  celsius <- temp - 273.15
  return(celsius)
}

# Converting Fahrenheit to Celsius
# by reusing the two functions you just wrote

far_to_cel <- function(far) {
  cel <- kel_to_cel(far_to_kel(far))
  return(cel)
}

# Not nesting the functions can be more readable

far_to_cel <- function(far) {
  if (!is.numeric(far)) {
    stop("Far must be a numeric value")
  }
  kel <- far_to_kel(far)
  cel <- kel_to_cel(kel)
  return(cel)
}

